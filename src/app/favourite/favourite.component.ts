import { Component, OnInit } from '@angular/core';
import { Favlist } from '../favlist';
import { FavBooks } from '../favbooks';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
  favlist: Favlist[] = [];
  fav=new FavBooks();
  meseage="";
  user="";
  constructor(private _service : ServiceService) { }

  ngOnInit(){ 
    this.getFavBookList();
    this.user=this._service.getMessage();
  }

  getFavBookList(){
    this._service.getFavBooksList().subscribe(data=>{
      this.favlist = data;
    });
  }

  removeBook(id){
    
    if(this._service.getMessage()==""){
      alert("Login With Proper Details")
      return
    }
    this.fav.id=id;
    this.fav.userEmail=this._service.getMessage();
    this._service.setBookid(id);
    console.log(this.fav);
    this._service.removefavlistFromRemote().subscribe(data=>{
      alert("Book Removed");
    });

  }
}