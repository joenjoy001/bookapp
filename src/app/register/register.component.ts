import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { User } from '../user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user= new User();
  msg='';
  message=""

  constructor(private _service: ServiceService, private _router : Router) { }

  ngOnInit() {
  }

  signUp(){
    if(this.user.emailId==null || this.user.password==null || this.user.username ==null){
      console.log("null");
      this.message="register failed";
      alert("Please enter Your details")
      return
    }
    this._service.registerUserFromRemote(this.user).subscribe(
      data =>{
        console.log("response recieved");
        this.msg="Registration successful";
        alert("Registration successful");
        this._router.navigate(['/login']);

      },
      error=>{
        this.message="Email Already Exist";
        console.log("exception Occured");
        this.msg=error.error;
        
      }
    )
    
  }}
