export class Books {
    id!: number;
    bookName!: string;
    bookAuthor!: string;
    bookUrl!: string;
    button!: string;
    buttondDisabled: boolean= false;
    constructor(){}
}