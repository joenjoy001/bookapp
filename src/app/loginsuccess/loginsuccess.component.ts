import { Component, OnInit } from '@angular/core';
import { Books } from '../books';
import { Favlist } from '../favlist';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-loginsuccess',
  templateUrl: './loginsuccess.component.html',
  styleUrls: ['./loginsuccess.component.css']
})
export class LoginsuccessComponent implements OnInit {

  books: Books[] = [];
  
  favlist= new Favlist();
  msg="";
  
  constructor(private _service : ServiceService) { }
  userid="";
  ngOnInit() {
    this.getBooks();
    this.userid=this._service.getMessage();
    
  }
  
  private getBooks(){
    this._service.getBooksList().subscribe(data=>{
      this.books = data;
    });
  }
  addtoList(bookName,bookAuthor,bookUrl){
    
    this.userid=this._service.getMessage();
    if(this._service.getMessage()==""){
      alert("Login With Proper Details")
      return
    }
    console.log(this.userid);
    this.favlist.bookName=bookName;
    this.favlist.bookAuthor=bookAuthor;
    this.favlist.userEmail=this._service.getMessage();
    this.favlist.bookUrl=bookUrl;
    this._service.addtofavlistFromRemote(this.favlist).subscribe(data=>{
      alert("Added to favourite");
    },
    error => {
      this.msg= "Book is Already in your List"
      alert(this.msg)
      console.log("Exeption Occuerd");
      
    })
  }
}