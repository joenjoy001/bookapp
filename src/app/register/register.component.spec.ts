import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceService } from '../service.service';

import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let serviceService: ServiceService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientModule, FormsModule, RouterTestingModule],
      declarations: [ RegisterComponent ],
      providers: [ServiceService]
    })
    .compileComponents();
    serviceService = TestBed.get(ServiceService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
