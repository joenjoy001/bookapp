import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Books } from './books';
import { User } from './user';
import { HttpClient } from '@angular/common/http';
import { Favlist } from './favlist';
import { FavBooks } from './favbooks';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  message="";
  bookid="";
  private baseURL = "http://localhost:9001/books";
  constructor(private _http : HttpClient) { }

  public loginUserFromRemote(user :User):Observable<any>{ // connecting with backend, spring
     console.log("inside");
     
     return this._http.post<any>("http://localhost:9000/loginuser",user)
  }

  public registerUserFromRemote(user :User):Observable<any>{
    return this._http.post<any>("http://localhost:9000/registeruser",user)
  }

  public addtofavlistFromRemote(favlist :Favlist):Observable<any>{
    return this._http.post<any>("http://localhost:9002/addtolist",favlist)
  }

  getBooksList(): Observable<Books[]>{
    return this._http.get<Books[]>(`${this.baseURL}`);
  }
  getFavBooksList(): Observable<FavBooks[]>{
    return this._http.get<FavBooks[]>(`http://localhost:9002/favlist/`+this.getMessage())
  }
  
  public removefavlistFromRemote(): Observable<FavBooks>{
    let url=`http://localhost:9002/favlist/`+this.getBookid();
    return this._http.delete<FavBooks>(url);
  }
  setMessage(data){ 
    this.message=data;
  }
  getMessage(){
    return this.message;
  }
  setBookid(data){
    this.bookid=data;
  }
  getBookid(){
    return this.bookid;
  }
}
