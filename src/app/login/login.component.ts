import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user= new User(); //object of user class
  msg='';
  userid="";
  
  
  constructor(private _service : ServiceService, private _router : Router) { }

  ngOnInit(){
  }

  loginUser(){
    
    if(this.user.emailId==null || this.user.password==null){   // if password or email null
      console.log("null");
      this.msg="login failed";
      alert("Please enter Your details")
      return  //exit from fun.
    }

    console.log(this.user);
    this._service.loginUserFromRemote(this.user).subscribe( // controls goes to service.service.ts
      data =>{ 
        console.log(this.user);
        console.log("response recieved");
        alert("login success")
        this.msg= "Login success";
        this.userid=this.user.emailId;
        this._service.setMessage(this.userid);
        this._router.navigate(['/loginsuccess']);
      },
      error => { //error msg
        this.msg= "Invalid Credentials, Please enter Email and Password"
        console.log("Exeption Occuerd");
        
      })
  }

}